﻿using System;

namespace test
{
    public class Personne
    {
        public static int population = 15;
        public static string KEY = "gbvredslbmze";

        private string _name = "";

        public Personne(string name)
        {
            this._name = name;
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
    }
}